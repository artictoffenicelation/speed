# Speed

Entry for [gotm.io Jam #2](https://itch.io/jam/gotm-jam-2).

# Credits

- [Orbitron](https://www.theleagueofmoveabletype.com/orbitron) font
- [Endesga-32](https://lospec.com/palette-list/endesga-32) palette
- [retro wave style track](https://freesound.org/people/JackyLaCracotte/sounds/515778/) by [JackyLaCracotte](https://freesound.org/people/JackyLaCracotte/)
