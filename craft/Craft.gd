extends KinematicBody
class_name Craft

const MAX_SPEED := 75.0
const ACCEL := 50.0
const BRAKE := 80.0
const MAX_TURN := 4.0
const TURN_RATE := PI / 2
const GRAVITY := 20.0
const GRIP := 3.0
const BOOST_SPEED := 50.0
const BOOST_ACCEL := 25.0
const BOOST_COST := 0.2

export(bool) var local_player := false

var velocity := Vector3.ZERO
var power := 1.0

var input_turn := 0.0
var input_accel := 0.0
var input_brake := 0.0
var input_boost := 0.0

func speed_factor() -> float:
	# TODO: translate velocity to local
	return abs(global_transform.basis.xform_inv(velocity).z / MAX_SPEED)

func grip_factor() -> float:
	return lerp(1.0, GRIP, speed_factor())

func signed_angle(v1: Vector3, v2: Vector3, axis: Vector3) -> float:
	var cross_to := v1.cross(v2);
	var unsigned_angle := atan2(cross_to.length(), v1.dot(v2));
	var signed := cross_to.dot(axis);
	return unsigned_angle * sign(signed)

# 0 is no drift
# -1 is full drift left
# 1 is full drift right
func drift_factor() -> float:
	if velocity.length() <= 5.0:
		return 0.0
	return signed_angle(velocity, global_transform.basis.z, global_transform.basis.y)

func _physics_process(delta: float):
	print(Input.get_action_strength("Accelerate"))
	if local_player:
		input_turn = (
			Input.get_action_strength("Turn Left") -
			Input.get_action_strength("Turn Right")
		)
		input_accel = Input.get_action_strength("Accelerate")
		input_brake = Input.get_action_strength("Brake")
		input_boost = Input.get_action_strength("Boost") and power > 0

	var turn := input_turn * TURN_RATE * delta
	rotate_y(turn)

	var facing := global_transform.basis.z.normalized()

	# bring velocity into alignment with facing proportional to grip
	# high grip means velocity quickly alings to facing
	# low grip means spending longer "sliding" in a different direction than the facing
	var target_vel := facing * velocity.length()
	velocity = velocity.linear_interpolate(target_vel, grip_factor() * delta)

	# now accelerate towards the speed input
	if input_boost:
		power = max(0, power - delta * BOOST_COST)

	var desired_vel := facing * (input_accel * MAX_SPEED + BOOST_SPEED * input_boost)
	var accel := ACCEL * (1 - speed_factor()) + input_boost * BOOST_ACCEL
	velocity = velocity.move_toward(desired_vel, accel * delta)
	velocity -= facing * input_brake * BRAKE * speed_factor() * delta
	velocity.y -= GRAVITY * delta

	var snap := Vector3.DOWN * 5
	velocity = move_and_slide_with_snap(velocity, snap, Vector3.UP)
