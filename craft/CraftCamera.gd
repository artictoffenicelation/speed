extends Camera

export(float, 0, 10) var min_z := 2.5
export(float, 0, 10) var max_z := 3.0

export(float, 0, 10) var min_y := 1.0
export(float, 0, 10) var max_y := 2.0

onready var craft: Craft = get_parent()

func _process(_delta: float):
	translation.z = -lerp(min_z, max_z, craft.speed_factor())
	translation.y = lerp(min_y, max_y, craft.speed_factor())
