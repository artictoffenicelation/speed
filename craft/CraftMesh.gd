extends MeshInstance

export(float, 0, 90) var max_tilt_degrees := 45.0

onready var craft: Craft = get_parent()

func _process(_delta: float):
	rotation_degrees.z = -craft.drift_factor() * max_tilt_degrees
