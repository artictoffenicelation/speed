extends AudioStreamPlayer3D

export(float) var slow_db := 0.0
export(float) var fast_db := 10.0
export(float) var slow_pitch := 1.0
export(float) var fast_pitch := 2.0

onready var craft: Craft = owner

func _process(_delta: float):
	unit_db = lerp(slow_db, fast_db, craft.speed_factor())
	pitch_scale = lerp(slow_pitch, fast_pitch, craft.speed_factor())
