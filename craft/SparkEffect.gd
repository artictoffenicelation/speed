extends RayCast

onready var particles: Particles = $Particles

func _physics_process(_delta: float):
	particles.emitting = is_colliding()
