extends Label

onready var craft: Craft = owner

func _process(_delta: float):
	text = "%d m/s" % craft.velocity.length()
