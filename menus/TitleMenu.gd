extends VBoxContainer

export(PackedScene) var next_scene: PackedScene

func _unhandled_input(ev: InputEvent):
	if ev is InputEventKey or ev is InputEventJoypadButton:
		get_tree().change_scene_to(next_scene)
